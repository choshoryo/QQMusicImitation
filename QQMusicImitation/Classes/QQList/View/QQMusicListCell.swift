//
//  QQMusicListCell.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/18.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

enum AnimationType {
    case Rotation
    case Translation
}

class QQMusicListCell: UITableViewCell {

    @IBOutlet weak var iconImageV: UIImageView!
    @IBOutlet weak var songLabel: UILabel!
    @IBOutlet weak var singerLabel: UILabel!
    
    /// 模型属性
    var musicModel: QQMusicModel? {
        didSet {
            if musicModel?.icon != nil {
                iconImageV.image = UIImage(named: (musicModel?.icon)!)
            }
            songLabel.text = musicModel?.name
            singerLabel.text = musicModel?.singer
        }
    }
    
    /// 快速创建Cell
    class func cell(tableView: UITableView) -> QQMusicListCell {
        let cellID = "MusicListCell"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellID) as? QQMusicListCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("QQMusicListCell", owner: nil, options: nil).first as? QQMusicListCell
        }
        
        return cell!
    }
    
    // MARK:- 系统回调
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // 裁剪图标为圆形
        iconImageV.layer.cornerRadius = iconImageV.width * 0.5
        iconImageV.layer.masksToBounds = true
    }

    // 取消cell的高亮状态
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        // 重写设置高亮操作为空
    }
    
}

// MARK:- 动画
extension QQMusicListCell {
    func beginAnimation(type:AnimationType) {
        switch type {
        case .Translation:
            self.layer.removeAnimationForKey("translation")
            let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
            animation.values = [60, 0]
            animation.duration = 0.2
            self.layer.addAnimation(animation, forKey: "translation")
        case .Rotation:
            self.layer.removeAnimationForKey("rotation")
            let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
            animation.values = [M_PI * 0.25, 0]
            animation.duration = 0.2
            self.layer.addAnimation(animation, forKey: "rotation")
        }
    }
}