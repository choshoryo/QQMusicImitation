//
//  QQMusicModel.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/18.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQMusicModel: NSObject {
    /** 歌曲名称 */
    var name: String?
    
    /** 歌曲文件名称 */
    var filename: String?
    
    /** 歌词文件名称  */
    var lrcname: String?
    
    /** 演唱者 */
    var singer: String?
    
    /** 歌手头像 */
    var singerIcon: String?
    
    /** 专辑图片 */
    var icon: String?
    
    /// 构造方法init
    override init() {
        super.init()
    }
    
    /// 构造方法initWithDict:
    init(dict: [String: AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        // 重写系统方法，对超出模型的字典键值对不做抛出异常处理
    }
}
