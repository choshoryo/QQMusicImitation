//
//  QQMusicListTVC.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/17.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQMusicListTVC: UITableViewController {

    // 模型数组
    var musicModelArr: [QQMusicModel]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupInterface()
        
        QQMusicDataTool.getMusicList { (musicModelArr) -> () in
            self.musicModelArr = musicModelArr
            // 给工具类传递播放列表
            QQMusicOperationTool.sharedInstance.musicList = musicModelArr
        }
        
    }

}

// MARK:- 数据展示
extension QQMusicListTVC {

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return musicModelArr?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = QQMusicListCell.cell(tableView)
        
        // 设置数据
        cell.musicModel = musicModelArr![indexPath.row]
        
        return cell
    }
    
    // 点击cell播放音乐
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let musicModel = musicModelArr![indexPath.row]
        QQMusicOperationTool.sharedInstance.playMusic(musicModel)
        
        // 跳转详情页面
        performSegueWithIdentifier("listToDetail", sender: nil)
    }
    
    // 动画展示cell
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let musicCell = cell as! QQMusicListCell
        musicCell.beginAnimation(.Translation)

    }
    
    // 改变cell的偏移量（效果）
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        // 获取屏幕可视范围内的所有cell的indexPath
        guard let indexPaths = tableView.indexPathsForVisibleRows else {return}
        
        let firstRow = indexPaths.first?.row ?? 0
        let lastRow = indexPaths.last?.row ?? 0
        let middleRow = Int(Double(firstRow + lastRow) * 0.5)
        
        for indexPath in indexPaths {
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            cell?.x = CGFloat(abs(indexPath.row - middleRow) * 25)
        }
    }
}

// MARK:- 界面搭建
extension QQMusicListTVC {
    /// 一次性设置界面
    private func setupInterface() {
        setupTableView()
        setupNavigationBar()
    }
    
    private func setupTableView() {
        tableView.backgroundView = UIImageView(image: UIImage(named: "QQListBack"))
        
        tableView.rowHeight = 60
        
        tableView.separatorStyle = .None
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBarHidden = true
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
}
