//
//  QQMusicLRCModel.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/19.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQMusicLRCModel: NSObject {
    var startTime: NSTimeInterval = 0
    var endTime: NSTimeInterval = 0
    var LRCStr: String = ""
}
