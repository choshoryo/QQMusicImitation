//
//  QQMusicMsgModel.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/18.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQMusicMsgModel: NSObject {
    
    var musicModel: QQMusicModel?
    
    var costTime: NSTimeInterval = 0
    
    var totalTime: NSTimeInterval = 0
    
    var isPlaying: Bool = false
    
    // 计算属性
    var costFormatTime: String {
        return QQMusicTimeTool.getFormattedTime(costTime)
    }
    var totalFormatTime: String {
        return QQMusicTimeTool.getFormattedTime(totalTime)
    }
}
