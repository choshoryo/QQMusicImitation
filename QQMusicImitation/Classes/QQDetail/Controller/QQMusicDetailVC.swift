//
//  QQMusicDetailVC.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/17.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

// MARK:- 属性
class QQMusicDetailVC: UIViewController {
    // 静态属性
    @IBOutlet weak var songLabel: UILabel!
    @IBOutlet weak var singerLabel: UILabel!
    @IBOutlet weak var foreImageView: UIImageView!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var LRCBackgroundView: UIScrollView!
    
    // 动态更新属性
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var totalPlayTime: UILabel!
    @IBOutlet weak var costPlayTime: UILabel!
    @IBOutlet weak var LRCLabel: QQLRCLabel!
    @IBOutlet weak var playOrPauseBtn: UIButton!
    
    // 私有属性
    private lazy var LRCVC: QQMusicLRCTVC = {
        let LRCVC = QQMusicLRCTVC()
        return LRCVC
    }()
    private var timer: NSTimer?
    private var displayLink: CADisplayLink?
    
    // 移除监听者
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}

// MARK:- 系统回调
extension QQMusicDetailVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOnceView()
        
        // 添加进度条手势
        let tapGesture = UITapGestureRecognizer(target: self, action: "tap:")
        progressSlider.addGestureRecognizer(tapGesture)
        
        // 添加监听通知者，播放完成后自动下一首
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "nextSongPlay", name: kAudioPlayerFinishPlayingNotificationName, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupMutiple()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        addTimer()
        addDisplayLink()
        setupOnceData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        removeTimer()
        removeDisplayLink()
    }
    
    // 设置状态栏
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        super.preferredStatusBarStyle()
        return .LightContent
    }
    
}

// MARK:- 界面
extension QQMusicDetailVC {
    
    // 调用一次的方法
    private func setupOnceView() {
        addLRCView()
        setupProgressSlider()
    }
    
    private func addLRCView() {
        LRCVC.tableView.backgroundColor = UIColor.clearColor()
        
        LRCBackgroundView.addSubview(LRCVC.tableView)
        LRCBackgroundView.pagingEnabled = true
        LRCBackgroundView.showsHorizontalScrollIndicator = false
        LRCBackgroundView.delegate = self
    }
    
    private func setupProgressSlider() {
        progressSlider.setThumbImage(UIImage(named: "player_slider_playback_thumb"), forState: .Normal)
    }
    
    // 调用n次的方法
    private func setupMutiple() {
        setupLRCViewFrame()
        setupForeImageView()
    }
    
    private func setupLRCViewFrame() {
        // 修改歌词展示空间的相关frame
        LRCVC.tableView.frame = LRCBackgroundView.bounds
        LRCVC.tableView.x = LRCBackgroundView.width
        LRCBackgroundView.contentSize = CGSize(width: LRCBackgroundView.width * 2, height: 0)
    }
    
    private func setupForeImageView() {
        foreImageView.layer.cornerRadius = foreImageView.width * 0.5
        foreImageView.layer.masksToBounds = true
    }
}

// MARK:- 动画
extension QQMusicDetailVC: UIScrollViewDelegate {
    
    private func addRotationAnimation() {
        // 移除之前的动画
        foreImageView.layer.removeAnimationForKey("rotation")
        // 添加动画
        let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        animation.values = [0, M_PI * 2]
        animation.duration = 30
        animation.repeatCount = MAXFLOAT
        // 设置播放完成之后, 不移除
        animation.removedOnCompletion = false

        foreImageView.layer.addAnimation(animation, forKey: "rotation")
        
    }
    
    private func resumeRotationAnimation() {
        addTimer()
        foreImageView.layer.resumeAnimate()
    }
    
    private func pauseRotationAnimation() {
        removeTimer()
        foreImageView.layer.pauseAnimate()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let alpha = 1 - scrollView.contentOffset.x / scrollView.width
        
        // 设置透明度
        foreImageView.alpha = alpha
        LRCLabel.alpha = alpha
    }
}

// MARK:- 业务逻辑
extension QQMusicDetailVC {
    @IBAction func playOrPause(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected {
            QQMusicOperationTool.sharedInstance.resumeCurrentMusic()
            resumeRotationAnimation()
        } else {
            QQMusicOperationTool.sharedInstance.pauseCurrentMusic()
            pauseRotationAnimation()
        }
    }
    
    @IBAction func preSongPlay() {
        QQMusicOperationTool.sharedInstance.playPreMusic()
        setupOnceData()
    }
    
    @IBAction func nextSongPlay() {
        QQMusicOperationTool.sharedInstance.playNextMusic()
        setupOnceData()
    }
    
    @IBAction func backClick(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}

// MARK:- 数据展示
extension QQMusicDetailVC {
    
    // 一次性数据
    private func setupOnceData() {
        let msgModel = QQMusicOperationTool.sharedInstance.updateMusicMessage()
        
        // 设置属性
        let musicName = msgModel.musicModel?.icon ?? ""
        backImageView.image = UIImage(named: musicName)
        foreImageView.image = UIImage(named: musicName)
        
        songLabel.text = msgModel.musicModel?.name
        singerLabel.text = msgModel.musicModel?.singer
        
        totalPlayTime.text = msgModel.totalFormatTime
        
        // 获取歌词数据源并交由歌词控制器控制
        let LRCDataModelArr = QQMusicLRCDataTool.getLRCData((msgModel.musicModel?.lrcname)!)
        LRCVC.datasource = LRCDataModelArr
        
        addRotationAnimation()
        
        if msgModel.isPlaying {
            resumeRotationAnimation()
        }else {
            pauseRotationAnimation()
        }
        
    }
    
    // 持续更新的数据
    func setupUpdatingData() {
        let msgModel = QQMusicOperationTool.sharedInstance.updateMusicMessage()
        
        costPlayTime.text = msgModel.costFormatTime
        progressSlider.value = Float(msgModel.costTime / msgModel.totalTime)
        
        // 更新播放或者暂停按钮
        playOrPauseBtn.selected = msgModel.isPlaying
    }
    
    func updateLRC() {
        let musicMsgModel = QQMusicOperationTool.sharedInstance.updateMusicMessage()
        
        let tuple = QQMusicLRCDataTool.getLRCRow(musicMsgModel.costTime, LRCModelArr: LRCVC.datasource!)
        
        LRCVC.scrollRow = tuple.row
        
        
        let LRCModel = tuple.LRCModel
        LRCLabel.text = LRCModel.LRCStr
        
        // 设置标签歌词渲染
//        guard LRCModel.endTime != 0 && LRCModel.startTime != 0 else {return}
        let progressValue = (musicMsgModel.costTime - LRCModel.startTime) / (LRCModel.endTime - LRCModel.startTime)
//        print(progressValue)
        LRCLabel.progress = progressValue
        LRCVC.progress = progressValue
        
        // 设置远程接收事件（因为内部信息中有当前播放时间，所以需要持续调用方法）
        let status = UIApplication.sharedApplication().applicationState
        if status.rawValue == 2 {
        QQMusicOperationTool.sharedInstance.setUpLockMessage()
        }
    }
}

// MARK:- 定时器
extension QQMusicDetailVC {
    private func addTimer() {
        timer = NSTimer(timeInterval: 1, target: self, selector: "setupUpdatingData", userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer!, forMode: NSRunLoopCommonModes)
    }
    
    private func removeTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    private func addDisplayLink() {
        let displayLink = CADisplayLink(target: self, selector: "updateLRC")
        displayLink.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)
    }
    
    private func removeDisplayLink() {
        displayLink!.invalidate()
        displayLink = nil
    }
}

// MARK:- 进度条操作
extension QQMusicDetailVC {
    
    // 按下的时候移除计时器
    @IBAction func sliderTouchDown(sender: UISlider) {
        removeTimer()
    }
    
    // 放手的时候添加计时器并播放对应进度的歌曲
    @IBAction func sliderTouchUp(sender: UISlider) {
        addTimer()
        
        // 根据进度条的值播放对应进度
        let musicMsgModel = QQMusicOperationTool.sharedInstance.updateMusicMessage()
        let currentTime = musicMsgModel.totalTime * Double(sender.value)
        QQMusicOperationTool.sharedInstance.seekTo(currentTime)
    }
    
    // 拖动时当前播放时间也会跟着变化
    @IBAction func sliderValueChanged(sender: UISlider) {
        let musicMsgModel = QQMusicOperationTool.sharedInstance.updateMusicMessage()
        let currentTime = musicMsgModel.totalTime * Double(sender.value)
        costPlayTime.text = QQMusicTimeTool.getFormattedTime(currentTime)
    }
    
    // 点击进度条跳到指定进度播放歌曲
    func tap(tapGesture: UITapGestureRecognizer) {
        let point = tapGesture.locationInView(progressSlider)
        progressSlider.value = Float(point.x / progressSlider.bounds.width)
        
        let musicMsgModel = QQMusicOperationTool.sharedInstance.updateMusicMessage()
        let currentTime = musicMsgModel.totalTime * Double(progressSlider.value)
        QQMusicOperationTool.sharedInstance.seekTo(currentTime)
    }
    
}

// MARK:- 接收远程播放事件
extension QQMusicDetailVC {
    
    // 摇一摇下一首歌
    override func motionBegan(motion: UIEventSubtype, withEvent event: UIEvent?) {
        nextSongPlay()
    }
    
    // 接收来自通知中心和上拉控制台的播放操作
    override func remoteControlReceivedWithEvent(event: UIEvent?) {
        let type = event!.subtype
        switch type {
        case .RemoteControlPlay:
            print("播放")
            QQMusicOperationTool.sharedInstance.resumeCurrentMusic()
        case .RemoteControlPause:
            print("暂停")
            QQMusicOperationTool.sharedInstance.pauseCurrentMusic()
        case .RemoteControlNextTrack:
            print("下一首")
            QQMusicOperationTool.sharedInstance.playNextMusic()
        case .RemoteControlPreviousTrack:
            print("上一首")
            QQMusicOperationTool.sharedInstance.playPreMusic()
        default:
            print("不处理")
        }
    }
}