//
//  QQMusicLRCTVC.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/19.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQMusicLRCTVC: UITableViewController {

    // 数据源
    var datasource: [QQMusicLRCModel]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    // 单行歌词进度
    var progress: Double = 0.0 {
        didSet {
            
            // 1. 获取到当前正在播放的cell
            let index = NSIndexPath(forRow: scrollRow, inSection: 0)
            let cell = tableView.cellForRowAtIndexPath(index) as? QQLRCCell
            
            // 2. 给cell, progress , 赋值
            cell?.progress = progress
            
        }
    }
    
    // 当前歌词行号
    var scrollRow: Int = 0 {
        didSet {
            // 防止重复滚动
            guard scrollRow != oldValue else {return}
            
            // 先刷新, 再做动画, 不然有问题
            tableView.reloadRowsAtIndexPaths(tableView.indexPathsForVisibleRows!, withRowAnimation: UITableViewRowAnimation.Fade)
            
            let indexPath = NSIndexPath(forRow: scrollRow, inSection: 0)
            tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Middle, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 设置tableView
        tableView.separatorStyle = .None
        tableView.allowsSelection = false

    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.contentInset = UIEdgeInsetsMake(tableView.height * 0.5, 0, tableView.height * 0.5, 0)
    }

    // MARK: - Table view data source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = datasource?.count else {return 0}
        return count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = QQLRCCell.cell(tableView)
        
        if indexPath.row == scrollRow {
            cell.progress = progress
        } else {
            cell.progress = 0.0
        }
        
        let LRCModel = datasource![indexPath.row]
        
        cell.LRCStr = LRCModel.LRCStr
        
        return cell
    }

}
