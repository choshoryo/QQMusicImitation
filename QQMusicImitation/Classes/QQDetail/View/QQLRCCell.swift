//
//  QQLRCCell.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/19.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQLRCCell: UITableViewCell {

    @IBOutlet weak var LRCContentLabel: QQLRCLabel!
    
    var progress: Double = 0.0 {
        didSet {
            LRCContentLabel.progress = progress
        }
    }
    
    var LRCStr: String = "" {
        didSet {
            LRCContentLabel.text = LRCStr
        }
    }
    
    /// 快速构造方法
    class func cell(tableView: UITableView) -> QQLRCCell {
        let cellID = "LRCCell"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellID) as? QQLRCCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("QQLRCCell", owner: nil, options: nil).first as? QQLRCCell
        }
        
        return cell!
    }

    
    
}
