//
//  QQLRCLabel.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/19.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQLRCLabel: UILabel {

    var progress: Double = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        // 填充字体颜色
        UIColor.greenColor().set()
        
        let progressCGFloat = CGFloat(progress)
        
        let fillRect = CGRectMake(rect.origin.x,  rect.origin.y, rect.size.width * progressCGFloat, rect.size.height)
        
        UIRectFillUsingBlendMode(fillRect, .SourceIn)
    }


}
