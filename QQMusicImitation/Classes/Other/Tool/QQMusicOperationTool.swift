//
//  QQMusicOperationTool.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/18.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

import MediaPlayer

// MARK:- 属性
class QQMusicOperationTool: NSObject {
    // 单例对象
    static let sharedInstance = QQMusicOperationTool()
    
    // 播放工具类
    private lazy var playerTool: QQMusicPlayerTool = {
        let playerTool = QQMusicPlayerTool()
        return playerTool
    }()
    
    // 音乐播放信息模型
    private lazy var musicMsgModel: QQMusicMsgModel = {
        let musicMsgModel = QQMusicMsgModel()
        return musicMsgModel
    }()
    
    // 当前音乐播放列表
    var musicList: [QQMusicModel]?
    
    // 当前播放歌曲的索引
    var index = 0 {
        didSet {
            guard musicList != nil else {return}
            
            if index < 0 {
                index = musicList!.count - 1
            }
            
            if index > musicList!.count - 1 {
                index = 0
            }
        }
    }
    
    // 远程播放模型
    private var artwork: MPMediaItemArtwork?
    
    // 绘制图行号
    private var drawRow: Int = -1
}

// MARK:- 播放操作
extension QQMusicOperationTool {
    func playMusic(musicModel: QQMusicModel) {
        
        let filename = musicModel.filename ?? ""
        playerTool.play(filename)
        
        if musicList == nil {
            print("没有播放列表, 只能, 播放一首歌曲")
            return
        }
        
        index = (musicList?.indexOf(musicModel))!
    }
    
    func resumeCurrentMusic() {
        playerTool.resume()
    }
    
    func pauseCurrentMusic() {
        playerTool.pause()
    }
    
    func playPreMusic() {
        index -= 1
        
        guard musicList != nil else {return}
        
        playMusic(musicList![index])
    }
    
    func playNextMusic() {
        index += 1
        
        guard musicList != nil else {return}
        
        playMusic(musicList![index])
        
    }
    
    func seekTo(timeInterval: NSTimeInterval) {
        playerTool.seekTo(timeInterval)
    }
}

// MARK:- 播放信息
extension QQMusicOperationTool {
    func updateMusicMessage() -> QQMusicMsgModel {
        
        musicMsgModel.musicModel = musicList?[index]
        
        musicMsgModel.costTime = playerTool.costTime() ?? 0
        
        musicMsgModel.totalTime = playerTool.totalTime() ?? 0
        
        musicMsgModel.isPlaying = playerTool.isPlaying() ?? false
        
        return musicMsgModel
    }
}

// MARK:- 锁屏播放
extension QQMusicOperationTool {
    
    
    func setUpLockMessage() -> () {
        
        let musicMsgModel = updateMusicMessage()
        
        
        // 展示锁屏信息
        
        // 1. 获取锁屏播放中心
        let playCenter = MPNowPlayingInfoCenter.defaultCenter()
        
        // 当前正在播放的歌词信息
        // 根据歌词文件名获得歌词模型数组
        let LRCModelArr = QQMusicLRCDataTool.getLRCData((musicMsgModel.musicModel?.lrcname)!)
        // 根据当前播放时间找到模型中对应的歌词
        let LRCRowModel = QQMusicLRCDataTool.getLRCRow(musicMsgModel.costTime, LRCModelArr: LRCModelArr!)
        
        let LRCModel = LRCRowModel.LRCModel
        
        // 1.1 创建一个字典
        let songName = musicMsgModel.musicModel?.name ?? ""
        let singerName = musicMsgModel.musicModel?.singer ?? ""
        let costTime = musicMsgModel.costTime
        let totalTime = musicMsgModel.totalTime
        
        if musicMsgModel.musicModel?.icon != nil
        {
            let image = UIImage(named: (musicMsgModel.musicModel?.icon)!)
            if image != nil
            {
                if drawRow != LRCRowModel.row {
                    // 绘制歌词&封面图片
                    drawRow = LRCRowModel.row
                    let newImage = QQImageTool.getNewImage(image!, str: LRCModel.LRCStr)
                    
                    artwork = MPMediaItemArtwork(image: newImage)
                    
                }
            }
            
        }
        
        var infoDic: [String: AnyObject] = [
            
            // 歌曲名称
            MPMediaItemPropertyAlbumTitle: songName,
            
            // 演唱者
            MPMediaItemPropertyArtist: singerName,
            
            // 当前播放时间
            MPNowPlayingInfoPropertyElapsedPlaybackTime: costTime,
            
            // 总时长
            MPMediaItemPropertyPlaybackDuration: totalTime,
            
            // 专辑图片 MPMediaItemArtwork
            //            MPMediaItemPropertyArtwork: artwork
        ]
        if artwork != nil
        {
            infoDic[MPMediaItemPropertyArtwork] = artwork!
        }
        
        // 2. 给锁屏中心赋值
        
        playCenter.nowPlayingInfo = infoDic
        
        // 3. 接收远程事件
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
    }
}