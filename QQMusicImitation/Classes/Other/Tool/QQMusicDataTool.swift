//
//  QQMusicDataTool.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/18.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQMusicDataTool: NSObject {
    class func getMusicList(result: (musicModelArr: [QQMusicModel]?) -> ()) {
        // 从本地的plist文件加载
        // 1. 获取文件的路径
        guard let path = NSBundle.mainBundle().pathForResource("Musics.plist", ofType: nil) else {
            result(musicModelArr: nil)
            return
        }
        
        // 2. 加载文件内容 dic array
        guard let dictArr = NSArray(contentsOfFile: path) else {
            result(musicModelArr: nil)
            return
        }
        
        // 3. 把字典转成模型
        var modelArr = [QQMusicModel]()
        for dict in dictArr {
            let musicModel = QQMusicModel(dict: dict as! [String : AnyObject])
            modelArr.append(musicModel)
        }
        
        // 4. 返回出去
        result(musicModelArr: modelArr)
    }
}
