//
//  QQMusicLRCDataTool.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/19.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQMusicLRCDataTool: NSObject {
    
    // 获得当前歌词的行号
    class func getLRCRow(currentTime: NSTimeInterval, LRCModelArr: [QQMusicLRCModel]) -> (row: Int, LRCModel: QQMusicLRCModel) {
        let count = LRCModelArr.count
        for i in 0..<count {
            let LRCModel = LRCModelArr[i]
            
            if currentTime >= LRCModel.startTime && currentTime < LRCModel.endTime {
//                print(i)
                return (i, LRCModel)
            }
        }
        return (0, QQMusicLRCModel())
    }
    
    // 获得解析好的歌词文件
    class func getLRCData(LRCFileName: String) -> [QQMusicLRCModel]? {
        // 解析歌词文件
        guard let path = NSBundle.mainBundle().pathForResource(LRCFileName, ofType: nil) else {return nil}
        
        // 加载歌词文件内容
        var LRCContent = ""
        do {
            LRCContent = try String(contentsOfFile: path)
        } catch {
            print(error)
            return nil
        }
        // 分割内容
        let LRCStrArr = LRCContent.componentsSeparatedByString("\n")
        var LRCModelArr = [QQMusicLRCModel]()
        for LRCStr in LRCStrArr {
            
            // 过滤垃圾数据
            if LRCStr.containsString("[ti:") || LRCStr.containsString("[ar:") || LRCStr.containsString("[al:") || LRCStr == "" {continue}
            
            let LRCModel = QQMusicLRCModel()
            LRCModelArr.append(LRCModel)
            // 解析每一行的内容
            let resultStr = LRCStr.stringByReplacingOccurrencesOfString("[", withString: "")
            
            let timeAndContent = resultStr.componentsSeparatedByString("]")
            
            if let startTime = QQMusicTimeTool.getTimeInterval(timeAndContent[0]) {
                LRCModel.startTime = startTime
            }
            
            LRCModel.LRCStr = timeAndContent[1]
        }
        
        // 关联歌词数组中的元素
        let count = LRCModelArr.count
        for i in 0..<count {
            guard i != count - 1 else {continue}
            LRCModelArr[i].endTime = LRCModelArr[i+1].startTime
        }
        
        // 装进数组返回
        return LRCModelArr
    }
}
