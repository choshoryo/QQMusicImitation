//
//  QQMusicPlayerTool.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/18.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

import AVFoundation

let kAudioPlayerFinishPlayingNotificationName = "finishPlayingAudioNotification"

class QQMusicPlayerTool: NSObject {
    
    override init() {
        super.init()
        
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback)
            
            try session.setActive(true)
        } catch {
            print(error)
            return
        }
    }
    
    // 播放器
    private var player: AVAudioPlayer?
    
    func play(name: String) {
        // 具体的播放实现
        // 1. 创建播放器
        guard let url = NSBundle.mainBundle().URLForResource(name, withExtension: nil) else {
            return
        }
        
        if url == player?.url {
            // 播放的是同一首歌曲
            player?.play()
            return
        }
        
        do {
            player = try AVAudioPlayer(contentsOfURL: url)
        } catch {
            print(error)
            return
        }
        
        // 2. 准备播放
        player?.prepareToPlay()
        
        // 3. 开始播放
        player?.play()
    }
    
    func resume() {
        player?.play()
    }
    
    func pause() {
        player?.pause()
    }
    
    func costTime() -> NSTimeInterval {
        return player!.currentTime
    }
    
    func totalTime() -> NSTimeInterval {
        return player!.duration
    }
    
    func isPlaying() -> Bool {
        return player!.playing
    }
    
    func seekTo(timeInterval: NSTimeInterval) {
        player?.currentTime = timeInterval
    }
}

// delegate method
extension QQMusicPlayerTool: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        
        // 通知外界播放完毕
        NSNotificationCenter.defaultCenter().postNotificationName(kAudioPlayerFinishPlayingNotificationName, object: nil)
    }
}
