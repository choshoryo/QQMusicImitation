//
//  QQMusicTimeTool.swift
//  QQMusicImitation
//
//  Created by Kevin Cheung on 16/5/19.
//  Copyright © 2016年 zero. All rights reserved.
//

import UIKit

class QQMusicTimeTool: NSObject {
    // 获得格式化时间字符串
    class func getFormattedTime(time: NSTimeInterval) -> String {
        let min = Int(time / 60)
        let sec = Int(time % 60)
        
        return String(format: "%02d:%02d", min, sec)
    }
    
    // 获得格式化时间（秒）
    class func getTimeInterval(formattedTime: String) -> NSTimeInterval? {
        let minAndSec = formattedTime.componentsSeparatedByString(":")
        if minAndSec.count == 2 {
            let min = NSTimeInterval(minAndSec[0]) ?? 0
            let sec = NSTimeInterval(minAndSec[1]) ?? 0
            return min * 60 + sec
        }
        return nil
    }
}
